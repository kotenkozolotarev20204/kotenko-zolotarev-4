#include <iostream>
#include <ctime>
#include <iomanip>
#include "MATRIX.h"

using namespace std;

void CheckSignAlternation(int** matrix, int matrixSize)
{
    int SignString;
    cout << "Input string number: ";
    cin >> SignString;

    bool check = true;
    char sign{ ' ' };
    for (int j = 0; j < matrixSize; j++)
    {
        if (sign == ' ')
        {
            if (matrix[SignString - 1][j] < 0)
            {
                sign = '-';
            }
            else
            {
                if (matrix[SignString - 1][j] > 0)
                {
                    sign = '+';
                }
            }
        }
        else
        {
            if ((sign == '+') && (matrix[SignString - 1][j] < 0) || ((sign == '-') && (matrix[SignString - 1][j] > 0)))
            {
                check = false;
                break;
            }
        }
    }

    if (check)
    {
        cout << "Not contain" << endl;
    }
    else
    {
        cout << "Contain" << endl;
    }
    system("pause");
    system("cls");
}