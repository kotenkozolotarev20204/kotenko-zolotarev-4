#ifndef MATRIX
#define MATRIX
int** CreateMatrix(int**&, int&);
void CheckSignAlternation(int**, int);
int** TransformMatrix(int**, int);
void SearchForFirstNull(int**, int);
void OutputMatrix(int**, int);
#endif