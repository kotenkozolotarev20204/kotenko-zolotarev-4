#include <iostream>
#include <ctime>
#include <iomanip>

using namespace std;

void OutputMatrix(int** matrix, int matrixSize)
{
    for (int i = 0; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            cout << setw(4) << matrix[i][j];
        }
        cout << endl;
    }

    system("pause");
}