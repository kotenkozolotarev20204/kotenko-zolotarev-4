#include <iostream>
#include <ctime>
#include "MATRIX.h"

using namespace std;

void main()
{
    srand(time(0));

    int** BaseMatrix = NULL;
    int matrixSize;

    char key;
    do
    {
        cout << "1. Creating a matrix." << endl;
        cout << "2. Check for sign alternation in a string." << endl;
        cout << "3. Matrix transformation." << endl;
        cout << "4. Search for the first null element." << endl;
        cout << "5. Output matrix." << endl;
        cout << "6. Exit." << endl;
        cout << "Input menu item: ";
        cin >> key;
        system("cls");

        switch (key)
        {
        case '1':
            BaseMatrix = CreateMatrix(BaseMatrix, matrixSize);
            break;

        case '2':
            OutputMatrix(BaseMatrix, matrixSize);
            CheckSignAlternation(BaseMatrix, matrixSize);
            break;

        case '3':
            OutputMatrix(BaseMatrix, matrixSize);
            OutputMatrix(TransformMatrix(BaseMatrix, matrixSize), matrixSize);
            system("cls");
            break;

        case '4':
            OutputMatrix(BaseMatrix, matrixSize);
            SearchForFirstNull(BaseMatrix, matrixSize);
            break;

        case '5':
            OutputMatrix(BaseMatrix, matrixSize);
            system("cls");
            break;

        case '6':
            exit(0);
            break;
        }
    } while (true);
}