﻿#include <iostream>
#include <ctime>
#include <iomanip>
#include "MATRIX.h"

using namespace std;

void SearchForFirstNull(int** matrix, int matrixSize)
{
    int StringNumber;
    cout << "Input number of string to find the first null element: ";
    cin >> StringNumber;

    int NullString, NullColumn;
    bool NullCheck{};
    for (int i = StringNumber - 1; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            if (matrix[i][j] == 0)
            {
                NullCheck++;
                NullString = i;
                NullColumn = j;
                break;
            }
        }
        if (NullCheck)
        {
            break;
        }
    }
    if (NullCheck)
    {
        cout << "String of the first null element: " << NullString + 1 << endl;
        cout << "Column of the first null element: " << NullColumn + 1 << endl;
    }
    else
    {
        cout << "There is not any null element" << endl;
    }

    system("pause");
    system("cls");
}