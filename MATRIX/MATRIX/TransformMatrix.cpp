#include <iostream>
#include <ctime>
#include <iomanip>

using namespace std;

int** TransformMatrix(int** matrix, int matrixSize)
{
    int DigitsSum, copy;
    for (int i = 0; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            DigitsSum = 0;
            copy = matrix[i][matrixSize - i - 1];
            while (copy)
            {
                DigitsSum += copy % 10;
                copy /= 10;
            }
            matrix[i][j] += abs(DigitsSum);
        }
    }

    return matrix;
}