#include <iostream>
#include <ctime>
#include <iomanip>

using namespace std;

int** CreateMatrix(int** &matrix, int &matrixSize)
{
    cout << "Input size of Matrix: ";
    cin >> matrixSize;

    int minimumNumber;
    cout << "Input min number of matrix: ";
    cin >> minimumNumber;

    int maximumNumber;
    cout << "Input max number of matrix: ";
    cin >> maximumNumber;

    matrix = new int* [matrixSize];
    for (int i = 0; i < matrixSize; i++)
    {
        matrix[i] = new int[matrixSize];
    }

    for (int i = 0; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            matrix[i][j] = minimumNumber + rand() % (maximumNumber - minimumNumber + 1);
        }
    }

    system("cls");
    return matrix;
}